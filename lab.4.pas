{Сформировать стек, поместив в него вещественные значения. Исключить
из стека числа, превышающие заданное значение.}
uses crt;
type 
ptrSteck = ^steck;
steck = record
  elem : real;
  old : ptrSteck;
  end;

var ourSteck :ptrSteck;
    num :real;
    maxLvl :real;
    
procedure push (var ourSteck :ptrSteck; num :real);
var newPtr :ptrSteck;
begin
new(newPtr);
if num <= maxLvl then begin
newPtr^.elem := num;
newPtr^.old := ourSteck;
ourSteck := newPtr;
dispose(newPtr);
end;
end;

procedure output (ourSteck :ptrSteck);
var newPtr :ptrSteck; 
begin
while ourSteck <> nil do
 begin
   newPtr := ourSteck;
  if newPtr^.elem <> 0 then 
  writeln(newPtr^.elem);
  ourSteck := ourSteck^.old;
 end;
end;

procedure destroy (var ourSteck :ptrSteck);
var newPtr :ptrSteck;
begin
while ourSteck <> nil do
 begin
  newPtr := ourSteck;
  ourSteck := ourSteck^.old;
  dispose(newPtr);
 end;
end;

begin
  clrscr;
  new(ourSteck);
  while(true) do
  begin
    writeln('введите действие \1 - введите максимальное значение \2 - добавить элемент \3 - извлечь элементы  \4 - выйти из программы');
    readln(num);
    if num = 1 then 
      begin
      writeln('введите максимальный элемент');
      read(maxLvl);
      end;
      if num = 2 then
    begin
      writeln('введите элемент');
      read(num);
      push(ourSteck, num);
      end
      else if num = 3 then 
    begin
      output(ourSteck);
      end
    else if num = 4 then begin 
    destroy(ourSteck);
    break;
 end;
 end;
end.
